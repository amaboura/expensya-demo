	$(document).ready(function(){
		$("#signin").click(function(){
			var email = $("#email").val();
			var password = $("#password").val();
			var data = JSON.stringify({email:email,password:password});
			$.ajax({
				url:"signin",
				type:"POST",
				data:data,
				contentType:"application/json",
				success:function(data,status,xhr){
					window.location.replace("profile");
				},
				error:function(xhr,status,err){
					console.log(err);
					alert("wrong credentials");
				}	

			})

		});
		
	});