/*
	Author : Mohamed Amine Aboura
	Version : 0.0.1
	Description : Expensya login protyotype
*/

// Node modules
var express = require("express");
var bodyParser = require("body-parser");
var request = require("request");
var path = require("path");
var cookieParser = require('cookie-parser');
var session = require('express-session');

// Initialize the app
var app = express();

// accept POST requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// path to static files
app.use("/static",express.static(path.normalize(path.join(__dirname,"/static"))));

// path to views
app.set("/views","/views");
// set the view engine (template engine)
app.set("view engine","jade");
// session config
app.use(cookieParser());
app.use(session({secret:"RMyE1Ck7o9WVtwYFqrFd",cookie: { maxAge: 60000 },saveUninitialized: true,resave: true}));

// session & request
var sess,r;

// http server port
var port = process.env.PORT || 8080; 

var router = express.Router();

// login request
router.route("/signin")

	.post(function(req,res){

		var email = req.body.email;
		var password = req.body.password;

		request(builder(email,password),function(err,resp,body){
			if (!err && resp.statusCode==200){
				var cl = resp.headers['content-length'];
				if(cl=="0")
				{
					res.status(400).end(); // wrong credentials
				}
				else
				{	r = req;
					sess = req.session;
					sess.email = email;
					res.status(200).end(); // logged in OK
				}
			}
		})

	})

	.get(function(req,res){
		// login page
		res.render('login');
	});

// profile page
router.route("/profile")

	.get(function(req,res){
	if(r.session){
		if (r.session.email){
			res.render('profile');
		}
		else
		{
			res.redirect('signin')
		}
	}
	else{
			res.redirect('signin')
	}
	});

// path to home page
router.route("/")
	.get(function(req,res){

		res.json({message:"Hello Expensya!"});
	});

// logout the session
router.route("/logout")
	.get(function(req,res){

		r.session.destroy(function(err){
			console.log(err)
			res.redirect('signin')
		});
	});

// request url builder
var builder = function(email,password){
	var url = "http://expensya-dev.cloudapp.net/NoteFraisService.svc/login/";
	var method = "login";
	return url+"?"+method+"="+email+"&pass="+password;
}
app.use("/api",router);

app.listen(port);

console.log("Server running at port: "+ port);